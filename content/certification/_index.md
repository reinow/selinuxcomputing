---
title: "Certification"
date: 2020-04-08T12:14:34+02:00
description: "Certification - Common Criteria Evaluation and Validation"
---

## Common Criteria Evaluation and Validation

For some classes of government and military users, security certification is an important procurement requirement. The integration of [SELinux](/selinux) made it possible to have Linux certified to the highest level currently achieved by mainstream operating systems. Systems based on Linux has been certified under the Common Criteria Evaluation and Validation Scheme (CCEVS) to Evaluation Assurance Level 4 Augmented (EAL4+), against the protection profiles:

* LSPP: Labeled Security Protection Proﬁle
* CAPP: Controlled Access Protection Proﬁle
* RBACPP: Role Based Access Control Protection Proﬁle

EAL4+ is the highest assurance level likely achievable without a specially designed operating system.
