---
title: "MAC"
date: 2020-03-23T12:14:34+06:00
description: "MAC - Mandatory Access Control"
---

## Mandatory Access Control
In computer security, *Mandatory Access Control* - MAC refers to a type of access control where the operating system controls the ability of a subject to access an object. A subject is usually a process or thread. Objects are typically directories, files, shared memory segments, TCP/UDP ports, or other processes. Subjects and objects each have security attributes. When a subject attempts to access an object, an authorization rule enforced by the kernel examines the security attributes and decides whether access is granted. Any operation by any subject on any object will be checked against the authorization rules to determine if the operation is allowed.

<div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-md-6 text-center">
                <figure>
                    <img class="img-responsive" src="/images/dac_en.png" alt="DAC - Discretionary Access Control" title="DAC - Discretionary Access Control" />
                    <figcaption>Discretionary Access Control</figcaption>
                </figure>
            </div>
            <div class="col-xs-12 col-md-6 text-center">
                <figure>
                    <img class="img-responsive" src="/images/mac_en.png" alt="MAC - Mandatory Access Control" title="MAC - Mandatory Access Control" />
                    <figcaption>Mandatory Access Control</figcaption>
                </figure>          
            </div>
</div>

## Traditional MAC
Traditional MAC mechanisms have been designed to mainly prevent information leaks, and confined to a multi-level security (MLS) policy which bases its decisions on the classification of objects and the clearances of subjects.

* Security levels on objects are called classifications
* Security levels on subjects are called clearances

This traditional approach is too limiting to address many security requirements in a modern computing world. MLS provides poor support for data and application integrity, separation of duty, and least privilege requirements. It fails to tightly control the relationship between a subject and the code it executes. This limits the ability of the system to offer protection based on the function and trustworthiness of the code, to correctly manage permissions required for execution, and to minimize the likelihood of malicious code execution.

## What can MAC offer?

* Strong separation of security domains
  * Separate data based on confidentiality/integrity/purpose
* System, application, and data integrity
  * Protect against unauthorized modifications
  * Prevent ill-formed modifications
* Ability to limit program privileges
  * Safely run code of uncertain trustworthiness
  * Prevent exploit of flaw in program from escalating privilege
  * Limit each program to only what is required for its purpose
* Processing pipeline guarantees
  * Ensure that data is processed as required
  * Split processing into small, minimally trusted stages
  * Encryption, sanitization, virus scanning
* Authorization limits for legitimate users
  * Decompose administrator role
  * Partition users into classes based on position, clearance, etc.

