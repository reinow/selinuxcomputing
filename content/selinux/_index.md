---
title: "SELinux"
date: 2020-03-23T12:14:34+06:00
description: "SELinux is a strong and ﬂexible Mandatory Access Control (MAC) framework for Linux."
---

## Security Enhanced Linux
SELinux is a strong and ﬂexible [Mandatory Access Control](/mac/) (MAC) framework for Linux. It is based on many years research on secure operating systems by the United States National Security Agency (NSA), and integrated into the Linux kernel via the Linux Security Modules API. SELinux makes it virtually impossible for unauthorized users or programs to take control over processes, files or hardware, thereby preventing unauthorized access to important information. All security relevant accesses between subjects and objects are strictly controlled according to a dynamically loaded mandatory security policy. Clean separation of enforcing mechanism and security policy provides considerable ﬂexibility in the implementation of security goals for the system, while fine granularity of control ensures complete mediation.

<img class="img-fluid rounded mx-auto d-block" style="margin-bottom: 40px;" src="https://www.selinuxcomputing.se/images/selinux_flow_en.jpg" alt="SELinux">

## Security Models
Any number of different security models may be combined by SELinux, with their complete effect being fully analysable. The default SELinux implementation is currently composed of the following security models:

* Type Enforcement (TE)
* Role Based Access Control (RBAC)
* Identity Based Access Control (IBAC)
* Multi Level Security (MLS)
* Multi Category Security (MCS)

These complement the standard Linux Discretionary Access Control (DAC) scheme.

## Our Offers
We provide SELinux solutions for any organization's computer infrastructure to prevent exploitation by all forms of malicious code, to ensure system integrity and that data is processed as required.

Our offers include:

* Development and implementation of SELinux security policies
* Hosting various types of Internet applications on SELinux servers, for example web and mail servers
* Managing secure SELinux servers for a broad range of applications


